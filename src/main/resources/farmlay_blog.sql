/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : farmlay_blog

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2017-10-13 17:44:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父节点ID',
  `type` tinyint(4) DEFAULT '1' COMMENT '类型 1-菜单 2-权限/按钮',
  `name` varchar(16) DEFAULT NULL COMMENT '菜单/权限名称',
  `href` varchar(128) DEFAULT NULL COMMENT '菜单链接',
  `target` varchar(32) DEFAULT NULL COMMENT '菜单链接目标，如_blank、_self等',
  `order_no` int(11) DEFAULT NULL COMMENT '排序序号',
  `icon` varchar(32) DEFAULT NULL COMMENT '图标',
  `permission` varchar(64) DEFAULT NULL COMMENT '权限标识',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态 0-隐藏 1-显示',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人ID',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` bigint(20) DEFAULT NULL COMMENT '修改人ID',
  `update_date` datetime DEFAULT NULL COMMENT '修改时间',
  `del_flag` tinyint(4) DEFAULT '0' COMMENT '删除标记 0-正常 1-已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COMMENT='系统菜单表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '0', '1', '系统管理', null, null, '10', 'fa-gears', null, '1', null, null, '1', '2017-10-12 17:53:01', '0');
INSERT INTO `sys_menu` VALUES ('2', '1', '1', '用户管理', '/admin/sys/user/', null, '20', 'fa-user', 'sys:user:view', '1', null, null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('3', '2', '2', '编辑', null, null, '30', null, 'sys:user:edit', '1', null, null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('4', '2', '2', '删除', null, null, '60', null, 'sys:user:delete', '1', null, null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('5', '1', '1', '菜单管理', '/admin/sys/menu/', null, '30', 'fa-th-list', 'sys:menu:view', '1', null, null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('6', '5', '2', '编辑', '', null, '30', '', 'sys:menu:edit', '1', null, null, '1', '2017-10-13 10:29:35', '0');
INSERT INTO `sys_menu` VALUES ('7', '5', '2', '删除', '', null, '60', 'a', 'sys:menu:delete', '1', '1', '2017-10-12 14:20:50', '1', '2017-10-13 10:28:39', '0');
INSERT INTO `sys_menu` VALUES ('8', '1', '1', '角色管理', '/admin/sys/role/', null, '40', 'fa-mortar-board', 'sys:role:view', '1', '1', '2017-10-12 16:16:58', '1', '2017-10-13 10:31:36', '0');
INSERT INTO `sys_menu` VALUES ('9', null, '2', '文章管理1111', null, null, '101', 'fa-battery-full', null, '0', '1', '2017-10-12 17:22:22', '1', '2017-10-12 17:54:44', '1');
INSERT INTO `sys_menu` VALUES ('10', '9', '1', '文章发布', null, null, '0', null, null, '1', '1', '2017-10-12 17:38:02', '1', '2017-10-12 17:38:02', '1');
INSERT INTO `sys_menu` VALUES ('11', null, '1', null, null, null, '2000', null, null, '1', '1', '2017-10-12 18:05:33', '1', '2017-10-12 18:05:33', '1');
INSERT INTO `sys_menu` VALUES ('12', null, '1', null, null, null, '3000', null, null, '1', '1', '2017-10-12 18:05:46', '1', '2017-10-12 18:05:46', '1');
INSERT INTO `sys_menu` VALUES ('13', '8', '2', '编辑', '', null, '30', '', 'sys:role:edit', '1', '1', '2017-10-13 10:29:20', '1', '2017-10-13 10:29:38', '0');
INSERT INTO `sys_menu` VALUES ('14', '8', '2', '删除', '', null, '60', '', 'sys:role:delete', '1', '1', '2017-10-13 10:30:01', '1', '2017-10-13 10:30:48', '0');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(32) DEFAULT NULL COMMENT '角色名称',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态 0-禁用 1-启用',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人ID',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` bigint(20) DEFAULT NULL COMMENT '修改人ID',
  `update_date` datetime DEFAULT NULL COMMENT '修改时间',
  `del_flag` tinyint(4) DEFAULT '0' COMMENT '删除标记 0-正常 1-已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COMMENT='系统角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '系统管理员', '1', '1', '2017-10-10 10:54:35', '1', '2017-10-10 10:54:39', '0');
INSERT INTO `sys_role` VALUES ('2', '用户管理', '1', '1', '2017-10-10 14:52:41', '1', '2017-10-13 15:09:58', '0');
INSERT INTO `sys_role` VALUES ('3', '用户管理', '1', null, null, '1', '2017-10-13 15:08:55', '1');
INSERT INTO `sys_role` VALUES ('4', '用户管理', '0', null, null, '1', '2017-10-13 15:09:20', '1');
INSERT INTO `sys_role` VALUES ('5', '用户管理1', '1', '1', '2017-10-13 15:10:57', '1', '2017-10-13 15:10:57', '1');
INSERT INTO `sys_role` VALUES ('6', '菜单管理', '1', '1', '2017-10-13 15:11:23', '1', '2017-10-13 15:11:23', '0');
INSERT INTO `sys_role` VALUES ('7', '角色管理', '1', '1', '2017-10-13 15:11:31', '1', '2017-10-13 15:11:31', '0');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色权限表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('2', '1');
INSERT INTO `sys_role_menu` VALUES ('2', '2');
INSERT INTO `sys_role_menu` VALUES ('2', '3');
INSERT INTO `sys_role_menu` VALUES ('2', '4');
INSERT INTO `sys_role_menu` VALUES ('4', '1');
INSERT INTO `sys_role_menu` VALUES ('4', '2');
INSERT INTO `sys_role_menu` VALUES ('4', '3');
INSERT INTO `sys_role_menu` VALUES ('4', '4');
INSERT INTO `sys_role_menu` VALUES ('4', '5');
INSERT INTO `sys_role_menu` VALUES ('4', '6');
INSERT INTO `sys_role_menu` VALUES ('4', '7');
INSERT INTO `sys_role_menu` VALUES ('5', '1');
INSERT INTO `sys_role_menu` VALUES ('5', '2');
INSERT INTO `sys_role_menu` VALUES ('5', '3');
INSERT INTO `sys_role_menu` VALUES ('5', '4');
INSERT INTO `sys_role_menu` VALUES ('5', '5');
INSERT INTO `sys_role_menu` VALUES ('5', '6');
INSERT INTO `sys_role_menu` VALUES ('5', '7');
INSERT INTO `sys_role_menu` VALUES ('5', '8');
INSERT INTO `sys_role_menu` VALUES ('5', '13');
INSERT INTO `sys_role_menu` VALUES ('5', '14');
INSERT INTO `sys_role_menu` VALUES ('6', '1');
INSERT INTO `sys_role_menu` VALUES ('6', '5');
INSERT INTO `sys_role_menu` VALUES ('6', '6');
INSERT INTO `sys_role_menu` VALUES ('6', '7');
INSERT INTO `sys_role_menu` VALUES ('7', '1');
INSERT INTO `sys_role_menu` VALUES ('7', '8');
INSERT INTO `sys_role_menu` VALUES ('7', '13');
INSERT INTO `sys_role_menu` VALUES ('7', '14');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `login_name` varchar(64) DEFAULT NULL COMMENT '登录名',
  `password` varchar(64) DEFAULT NULL COMMENT '密码',
  `name` varchar(32) DEFAULT NULL COMMENT '姓名',
  `email` varchar(128) DEFAULT NULL COMMENT '电子邮箱',
  `mobile` varchar(32) DEFAULT NULL COMMENT '手机号码',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态 0-禁用 1-启用',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人ID',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` bigint(20) DEFAULT NULL COMMENT '修改人ID',
  `update_date` datetime DEFAULT NULL COMMENT '修改时间',
  `del_flag` tinyint(4) DEFAULT '0' COMMENT '删除标记 0-正常 1-已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COMMENT='系统用户表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', '02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032', '超级管理员', null, null, '1', null, null, '1', '2017-10-10 14:59:10', '0');
INSERT INTO `sys_user` VALUES ('2', 'lxz', '02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032', '测试', 'aaa@qq.com', '13414221733', '1', '1', '2017-10-09 15:32:40', '1', '2017-10-09 15:32:44', '1');
INSERT INTO `sys_user` VALUES ('3', null, '009c5897605265defda163eb1e162b7f55adcf3063c78376d394fd54', null, null, null, '1', null, null, null, null, '1');
INSERT INTO `sys_user` VALUES ('4', null, 'fda986e167566fbf9ca63b37d54f74cc75148249c233e462ec84bf0c', null, null, null, '1', null, null, null, null, '1');
INSERT INTO `sys_user` VALUES ('5', 'aaa', '05b6b3f5ee5cdb930b1bd3b91e00d1f2f96d4101a7124ba258975c81', 'ads', 'dds@qqq.com', 'dasd', '1', '1', '2017-10-10 13:34:00', '1', '2017-10-10 13:34:00', '1');
INSERT INTO `sys_user` VALUES ('6', null, 'b5c27726a39005c1e415f9bc7ed2bfe15aebc1052ef18e5f4005e626', null, null, null, '0', '1', '2017-10-10 13:49:48', '1', '2017-10-10 13:51:40', '1');
INSERT INTO `sys_user` VALUES ('7', 'aaadd', '63d9a3a71ef7ffe406a4a12a16a5a10fb16f66e8c6416e3e6ee76315', 'adasd', '357888717@qq.com', '15250995150', '1', '1', '2017-10-10 14:14:45', '1', '2017-10-10 14:53:19', '1');
INSERT INTO `sys_user` VALUES ('8', 'luoji1', '9ae004c759b49bb2add52450207c7e85e32391dce1adf1202b030ca4', '逻辑', 'huijisi@163.com', '15250995150', '1', '1', '2017-10-10 14:56:49', '1', '2017-10-10 16:18:30', '1');
INSERT INTO `sys_user` VALUES ('9', 'dwqd', 'b367381726efd9aca53092685318737ff0ca8ab0be58636b0d6cd81c', '飞凡网', null, null, '1', '1', '2017-10-10 16:08:39', '1', '2017-10-10 16:08:39', '1');
INSERT INTO `sys_user` VALUES ('10', 'user', 'ad14c057a3d077dbcb2d04dcf72ec4954ffd178ad6f6d6a99f8e2a04', '用户管理', null, null, '1', '1', '2017-10-13 15:14:44', '1', '2017-10-13 15:14:44', '0');
INSERT INTO `sys_user` VALUES ('11', 'menu', '007df9c8b92c7e4d51b51795c7cf1cb42d797df0b79f2a750c3e619d', '菜单管理', null, null, '1', '1', '2017-10-13 15:17:12', '1', '2017-10-13 15:18:05', '0');
INSERT INTO `sys_user` VALUES ('12', 'role', 'f1f4c197e9381362f0efa92284248c4cf6579f72a11f122bb9408010', '角色管理', null, null, '1', '1', '2017-10-13 15:18:28', '1', '2017-10-13 15:18:28', '0');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户角色表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1');
INSERT INTO `sys_user_role` VALUES ('1', '2');
INSERT INTO `sys_user_role` VALUES ('7', '1');
INSERT INTO `sys_user_role` VALUES ('7', '2');
INSERT INTO `sys_user_role` VALUES ('8', '1');
INSERT INTO `sys_user_role` VALUES ('8', '2');
INSERT INTO `sys_user_role` VALUES ('9', '1');
INSERT INTO `sys_user_role` VALUES ('9', '2');
INSERT INTO `sys_user_role` VALUES ('10', '2');
INSERT INTO `sys_user_role` VALUES ('11', '6');
INSERT INTO `sys_user_role` VALUES ('12', '7');


-- ----------------------------
-- Table structure for cms_article
-- ----------------------------
DROP TABLE IF EXISTS `cms_article`;
CREATE TABLE `cms_article` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `category_id` bigint(20) DEFAULT NULL COMMENT '所属分类ID',
  `tags` varchar(128) DEFAULT NULL COMMENT '标签',
  `title` varchar(100) NOT NULL COMMENT '标题',
  `short_title` varchar(64) DEFAULT NULL COMMENT '自定义唯一短标识',
  `description` varchar(255) DEFAULT NULL COMMENT '说明',
  `content` mediumtext COMMENT '内容',
  `author` varchar(64) DEFAULT NULL COMMENT '作者',
  `thumbnail` varchar(100) DEFAULT NULL COMMENT '缩略图路径',
  `link_url` varchar(1000) DEFAULT NULL COMMENT '外链地址',
  `praise_total` bigint(20) DEFAULT '0' COMMENT '点赞总数',
  `view_total` bigint(20) DEFAULT '0' COMMENT '评价总数',
  `share_count` bigint(20) DEFAULT '0' COMMENT '分享次数',
  `read_count` bigint(20) DEFAULT '0' COMMENT '阅读次数',
  `status` char(1) DEFAULT '1' COMMENT '发布状态 0-未发布 1-已发布',
  `allow_comment` char(1) DEFAULT '1' COMMENT '是否允许评论 0-否 1-是',
  `sort` int(11) DEFAULT '0' COMMENT '排序序号',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` bigint(20) DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 NOT NULL DEFAULT '0' COMMENT '删除标记（0：正常；1：删除）',
  PRIMARY KEY (`id`),
  KEY `article_del_flag` (`del_flag`),
  KEY `article_short_title` (`short_title`),
  KEY `article_category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='文章';

-- ----------------------------
-- Table structure for cms_article_tags
-- ----------------------------
DROP TABLE IF EXISTS `cms_article_tags`;
CREATE TABLE `cms_article_tags` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `article_id` bigint(20) DEFAULT NULL COMMENT '文章id',
  `tag_id` bigint(20) DEFAULT NULL COMMENT '标签ID',
  PRIMARY KEY (`id`),
  KEY `Index_article_id` (`article_id`),
  KEY `Index_tag_id` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='文章标签关系表';


-- ----------------------------
-- Table structure for cms_category
-- ----------------------------
DROP TABLE IF EXISTS `cms_category`;
CREATE TABLE `cms_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '分类ID',
  `name` varchar(64) DEFAULT NULL COMMENT '分类名称',
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父分类ID',
  `sort` int(11) DEFAULT NULL COMMENT '排序序号',
  `status` char(1) DEFAULT '1' COMMENT '显示状态 0-隐藏 1-显示',
  `remarks` varchar(200) DEFAULT NULL COMMENT '备注',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` bigint(20) DEFAULT NULL COMMENT '修改人',
  `update_date` datetime DEFAULT NULL COMMENT '修改时间',
  `del_flag` char(1) CHARACTER SET utf8 NOT NULL DEFAULT '0' COMMENT '删除标记（0：正常；1：删除）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for cms_comment
-- ----------------------------
DROP TABLE IF EXISTS `cms_comment`;
CREATE TABLE `cms_comment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `article_id` bigint(20) DEFAULT NULL COMMENT '文章id',
  `reply_id` bigint(20) DEFAULT NULL COMMENT '回复评论记录ID',
  `name` varchar(32) DEFAULT NULL COMMENT '评论人姓名',
  `email` varchar(128) DEFAULT NULL COMMENT '评论人邮箱',
  `web_url` varchar(128) DEFAULT NULL COMMENT '评论人网址',
  `content` varchar(1000) DEFAULT NULL COMMENT '评论内容',
  `create_date` datetime DEFAULT NULL COMMENT '评论时间',
  PRIMARY KEY (`id`),
  KEY `Index_article_id` (`article_id`),
  KEY `Index_reply_id` (`reply_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='文章评论记录';

-- ----------------------------
-- Table structure for cms_tag
-- ----------------------------
DROP TABLE IF EXISTS `cms_tag`;
CREATE TABLE `cms_tag` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(32) DEFAULT NULL COMMENT '标签名称',
  `sort` int(11) DEFAULT '0' COMMENT '排序序号',
  `is_recommend` char(1) DEFAULT '0' COMMENT '是否推荐 1-是 0-否',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Index_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='文章标签';



-- ----------------------------
-- Function structure for getMenuChildList
-- ----------------------------
DROP FUNCTION IF EXISTS `getMenuChildList`;
DELIMITER ;;
CREATE FUNCTION `getMenuChildList`(rootId LONG) RETURNS varchar(1000) CHARSET utf8mb4
BEGIN
       DECLARE sTemp VARCHAR(1000);
       DECLARE sTempChd VARCHAR(1000);   
       SET sTemp = '';
       SET sTempChd =CAST(rootId AS CHAR);
       WHILE sTempChd IS NOT NULL DO
         SET sTemp = CONCAT(sTemp,',',sTempChd);
         SELECT GROUP_CONCAT(id) INTO sTempChd FROM sys_menu WHERE FIND_IN_SET(parent_id,sTempChd)>0;
       END WHILE;
       RETURN sTemp;
    END
;;
DELIMITER ;


-- ----------------------------
-- Function structure for getArticleCateChildList
-- ----------------------------
DROP FUNCTION IF EXISTS `getArticleCateChildList`;
DELIMITER ;;
CREATE FUNCTION `getArticleCateChildList`(rootId LONG) RETURNS varchar(1000) CHARSET utf8mb4
BEGIN
       DECLARE sTemp VARCHAR(1000);
       DECLARE sTempChd VARCHAR(1000);
       SET sTemp = '';
       SET sTempChd =CAST(rootId AS CHAR);
       WHILE sTempChd IS NOT NULL DO
         SET sTemp = CONCAT(sTemp,',',sTempChd);
         SELECT GROUP_CONCAT(id) INTO sTempChd FROM cms_category WHERE FIND_IN_SET(parent_id,sTempChd)>0;
       END WHILE;
       RETURN sTemp;
    END
;;
DELIMITER ;
