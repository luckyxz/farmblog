package com.farmlay.modules.cms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.farmlay.modules.cms.entity.CmsArticleTags;

/**
 * <p>
 * 文章标签关系表 服务类
 * </p>
 *
 * @author luckyxz999
 * @since 2017-11-02
 */
public interface ICmsArticleTagsService extends IService<CmsArticleTags> {
	
}
