package com.farmlay.modules.cms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.farmlay.modules.cms.entity.CmsArticleTags;
import com.farmlay.modules.cms.mapper.CmsArticleTagsMapper;
import com.farmlay.modules.cms.service.ICmsArticleTagsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 文章标签关系表 服务实现类
 * </p>
 *
 * @author luckyxz999
 * @since 2017-11-02
 */
@Service
public class CmsArticleTagsServiceImpl extends ServiceImpl<CmsArticleTagsMapper, CmsArticleTags> implements ICmsArticleTagsService {
	
}
