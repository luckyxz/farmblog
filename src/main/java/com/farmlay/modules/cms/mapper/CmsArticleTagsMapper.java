package com.farmlay.modules.cms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.farmlay.modules.cms.entity.CmsArticleTags;
import org.springframework.stereotype.Repository;

/**
 * <p>
  * 文章标签关系表 Mapper 接口
 * </p>
 *
 * @author luckyxz999
 * @since 2017-11-02
 */
@Repository
public interface CmsArticleTagsMapper extends BaseMapper<CmsArticleTags> {

}