package com.farmlay.modules.cms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.farmlay.modules.cms.entity.CmsComment;
import org.springframework.stereotype.Repository;

/**
 * <p>
  * 文章评论记录 Mapper 接口
 * </p>
 *
 * @author luckyxz999
 * @since 2017-11-16
 */
@Repository
public interface CmsCommentMapper extends BaseMapper<CmsComment> {

}