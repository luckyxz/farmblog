package com.farmlay.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.farmlay.modules.sys.entity.SysLog;
import com.farmlay.modules.sys.mapper.SysLogMapper;
import com.farmlay.modules.sys.service.ISysLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 后台操作日志表 服务实现类
 * </p>
 *
 * @author luckyxz999
 * @since 2017-10-16
 */
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog> implements ISysLogService {
	
}
