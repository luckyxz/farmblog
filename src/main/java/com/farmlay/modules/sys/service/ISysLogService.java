package com.farmlay.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.farmlay.modules.sys.entity.SysLog;

/**
 * <p>
 * 后台操作日志表 服务类
 * </p>
 *
 * @author luckyxz999
 * @since 2017-10-16
 */
public interface ISysLogService extends IService<SysLog> {
	
}
