package com.farmlay.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.farmlay.modules.sys.entity.SysUserRole;
import com.farmlay.modules.sys.mapper.SysUserRoleMapper;
import com.farmlay.modules.sys.service.ISysUserRoleService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色表 服务实现类
 * </p>
 *
 * @author luckyxz999
 * @since 2017-09-19
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {
	
}
