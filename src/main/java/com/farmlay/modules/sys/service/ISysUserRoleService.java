package com.farmlay.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.farmlay.modules.sys.entity.SysUserRole;

/**
 * <p>
 * 用户角色表 服务类
 * </p>
 *
 * @author luckyxz999
 * @since 2017-09-19
 */
public interface ISysUserRoleService extends IService<SysUserRole> {
	
}
