package com.farmlay.modules.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.farmlay.modules.sys.entity.SysUserRole;
import org.springframework.stereotype.Repository;

/**
 * <p>
  * 用户角色表 Mapper 接口
 * </p>
 *
 * @author luckyxz999
 * @since 2017-09-19
 */
@Repository
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}