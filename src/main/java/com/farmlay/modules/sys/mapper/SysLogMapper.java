package com.farmlay.modules.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.farmlay.modules.sys.entity.SysLog;

/**
 * <p>
  * 后台操作日志表 Mapper 接口
 * </p>
 *
 * @author luckyxz999
 * @since 2017-10-16
 */
public interface SysLogMapper extends BaseMapper<SysLog> {

}