package com.farmlay.modules.sys.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.farmlay.common.base.SuperController;
import com.farmlay.common.result.MsgResult;
import com.farmlay.common.result.PageResult;
import com.farmlay.common.utils.StringUtils;
import com.farmlay.modules.sys.entity.SysLog;
import com.farmlay.modules.sys.entity.SysUser;
import com.farmlay.modules.sys.service.ISysLogService;
import com.farmlay.modules.sys.service.ISysUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 * 系统日志表 前端控制器
 * </p>
 *
 * @author luckyxz999
 * @since 2017-10-16
 */
@Controller
@RequestMapping("${adminPath}/sys/log")
public class SysLogController extends SuperController {
	private final ISysLogService sysLogService;
    private final ISysUserService sysUserService;

    @Autowired
    public SysLogController(ISysLogService sysLogService, ISysUserService sysUserService) {
        this.sysLogService = sysLogService;
        this.sysUserService = sysUserService;
    }

    @RequiresPermissions("sys:log:view")
    @RequestMapping("")
    String user(Model model, @RequestParam Map<String, String> params) {
        model.addAttribute("params", params);
        model.addAttribute("logList", null);
        return "admin/sys/log/logList";
    }

    @RequiresPermissions("sys:log:view")
    @RequestMapping("/list")
    @ResponseBody
    PageResult list(@RequestParam Map<String, String> params, Integer pageNo, Integer pageSize) {
        QueryWrapper<SysLog> wrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(params.get("title"))) {
            wrapper.like("title", params.get("title"));
        }
        wrapper.orderByDesc("create_date");
        if (pageSize == null) {
            pageSize = 10;
        }
        if (pageNo == null) {
            pageNo = 1;
        } else {
            pageNo = pageNo / pageSize + 1;//页面上传过来的是offset，需要转成页码
        }
        //分页查询数据
        IPage<SysLog> logPage = new Page<>(pageNo, pageSize);
        wrapper.select("id", "title", "remote_addr", "user_agent", "request_uri", "method", "create_by", "create_by_name", "create_date");
        logPage = sysLogService.page(logPage, wrapper);
        return new PageResult(logPage.getRecords(), logPage.getTotal());
    }

    @RequiresPermissions("sys:log:view")
    @GetMapping("/view")
    String view(Model model, Long id) {
        if (id != null) {
            SysLog sysLog = sysLogService.getById(id);
            model.addAttribute("log", sysLog);

            if (sysLog != null && sysLog.getCreateBy() != null) {
                SysUser sysUser = sysUserService.getById(sysLog.getCreateBy());
                if (sysUser != null) {
                    sysLog.setCreateByName(sysUser.getName());
                }
            }
        }

        return "admin/sys/log/logView";
    }

    @RequiresPermissions("sys:log:delete")
    @PostMapping("/delete")
    @ResponseBody
    MsgResult delete(Long id) {
        if (sysLogService.removeById(id)) {
            return MsgResult.ok();
        }
        return MsgResult.error();
    }
}
